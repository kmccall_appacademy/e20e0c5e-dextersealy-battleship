class HumanPlayer

  def get_play
    print "What's your move: "
    gets.split(",").map { |str| str.strip.to_i - 1}
  end

end
