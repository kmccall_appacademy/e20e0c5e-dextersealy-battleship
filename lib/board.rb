class Board
  attr_reader :grid

  def initialize(grid=nil)
    @grid = grid ? grid : Board::default_grid
  end

  def self.default_grid
    10.times.map { [nil]*10 }
  end

  def empty?(pos=nil)
    pos ? self[pos].nil? : count == 0
  end

  def full?
    grid.flatten.none?(&:nil?)
  end

  def won?
    count == 0
  end

  def place_random_ship
    free_cells = grid.flatten.map.with_index {|el, idx| el.nil? ? idx : nil }
    pos = free_cells.compact.sample
    mark([pos / grid.length, pos % grid.length], :s)
  end

  def count
    grid.flatten.count { |el| el == :s }
  end

  def mark(pos, mark)
    raise "invalid position" unless valid?(pos)
    row, col = pos
    grid[row][col] = mark
  end

  def valid?(pos)
    pos.all? { |el| (0...grid.length) === el }
  end

  def [](pos)
    row, col = pos
    grid[row][col]
  end

  def display
    grid.each do |arr|
      puts arr.map { |sym| (sym.nil? || sym == :s) ? "." : sym.to_s }.join
    end
  end

end
