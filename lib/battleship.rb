require_relative 'board'
require_relative 'player'

class BattleshipGame
  attr_reader :board, :player

  def initialize(player=nil, board=nil)
    @player = player
    @board = board
  end

  def play
    until game_over?
      display_status
      hit = play_turn
      puts hit ? "Hit!" : "You missed!"
    end

    puts "Game over!"
    display_status
  end

  def game_over?
    board.won?
  end

  def display_status
    ships = (count == 1) ? "ship" : "ships"
    puts "#{count} #{ships} left"
    board.display
  end

  def count
    board.count
  end

  def play_turn
    attack(player.get_play)
  end

  def attack(pos)
    board.mark(pos, board.empty?(pos) ? :x : :*)
  end

end

if __FILE__ == $PROGRAM_NAME

  grid = 4.times.map { [nil]*4 }
  board = Board.new(grid)
  4.times { board.place_random_ship }

  player = HumanPlayer.new
  game = BattleshipGame.new(player, board)

  game.play
end
